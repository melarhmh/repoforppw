from django.urls import path
from . import views

app_name = "story4app"

urlpatterns = [
	path('', views.home, name="home"),
	path('playlist', views.playlist, name="playlist"),
	path('signup', views.signup, name="signup")
]
