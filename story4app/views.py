from django.shortcuts import render, redirect
from django.http import HttpResponse

# Create your views here.
def home(request):
    return render(request, "story3.html", {})

def playlist(request):
    return render(request, "story3_playlist.html", {})

def signup(request):
    return render(request, "story3_signup.html", {})