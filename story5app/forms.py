from django import forms
from .models import Portfolio

class PortfolioForm(forms.Form):
	status_attrs = {
	    'type':'date',
	    'placeholder':'YYYY-MM-DD',
	}	
	title = forms.CharField(label="Judul")
	date_start = forms.DateField(label="Tanggal Mulai", widget=forms.DateInput(attrs=status_attrs))
	date_end = forms.DateField(label="Tanggal Selesai", widget=forms.DateInput(attrs=status_attrs))
	desc = forms.CharField(label="Deskripsi Singkat Proyek")
	place = forms.CharField(label="Tempat")
	category = forms.CharField(label="Kategori Proyek")