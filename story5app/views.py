from django.shortcuts import render, redirect
from .forms import PortfolioForm
from .models import Portfolio

# Create your views here.

def portfolio(request):
    response = {}
    portfolios = Portfolio.objects.all().values()
    response = {
        "portfolios" : portfolios
    }
    return render(request, 'story5_portfoliolist.html', response)

def portfolioform(request):
    form = PortfolioForm(request.POST or None)
    response = {}
    if(request.method == "POST"):
        if (form.is_valid()):
            title = request.POST.get("title")
            date_start = request.POST.get("date_start")
            date_end = request.POST.get("date_end")
            desc = request.POST.get("desc")
            place = request.POST.get("place")
            category = request.POST.get("category")
            Portfolio.objects.create(title=title, date_start=date_start, date_end=date_end, desc=desc, place=place, category=category)
            return redirect('/portfolio')
        else:
            return render(request, 'story5_portfolioform.html', response)
    else:
        response['form'] = form
        return render(request, 'story5_portfolioform.html', response)

def delete_portfolio(request):
    Portfolio.objects.filter().delete()
    return redirect('/portfolio')