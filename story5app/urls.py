from django.urls import path
from . import views

app_name = "story5app"

urlpatterns = [
	path('portfolioform', views.portfolioform, name="portfolioform"),
	path('portfolio', views.portfolio, name="portfolio"),
	path('deleteportfolio', views.delete_portfolio, name="delete_portfolio")
]