from django.db import models

# Create your models here.

class Portfolio(models.Model):
    title = models.CharField(max_length=30)
    date_start = models.DateTimeField()
    date_end = models.DateTimeField()
    desc = models.CharField(max_length=100)
    place = models.CharField(max_length=100)
    category = models.CharField(max_length=30)